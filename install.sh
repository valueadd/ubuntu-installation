#!/bin/bash
# Assumption that this script is used only on ubuntu

{ # this ensures the entire script is downloaded #

if [[ -z "$SUDO_USER" ]] || [[ "$EUID" != "0" ]]; then
	echo "Please run this script using sudo"
	exit 1
fi

set -eu #x
export DEBIAN_FRONTEND=noninteractive

DOCKER_COMPOSE_VERSION=1.25.4
NODE_VERSION=node_13.x
PHP_VERSION=7.4

. /etc/lsb-release
DISTRO=$DISTRIB_CODENAME

PRE_INSTALL_PKGS="apt-transport-https \
          ca-certificates \
          curl \
          gnupg-agent \
          software-properties-common \
          gnupg"

POST_INSTALL_PKGS="google-chrome-stable \
          terminator \
          docker-ce \
          docker-ce-cli \
          containerd.io \
          git "

GPG_KEYS=("https://dl.google.com/linux/linux_signing_key.pub" \
          "https://download.docker.com/linux/ubuntu/gpg")

REPOSITORIES=("deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main"
          "universe" # Terminator
          "deb [arch=amd64] https://download.docker.com/linux/ubuntu $DISTRO stable" )

SNAP_PACKAGES="postman"
SNAP_PACKAGES_CLASSIC="slack"


# Question #1
echo
read -p "Do you want to install nodejs [y/N] " -r # -n 1
# echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
	GPG_KEYS+=("https://deb.nodesource.com/gpgkey/nodesource.gpg.key")
	REPOSITORIES+=("deb https://deb.nodesource.com/$NODE_VERSION $DISTRO main")
	POST_INSTALL_PKGS="$POST_INSTALL_PKGS nodejs"
fi

# Question #2
echo
read -p "Do you want to install php [y/N] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
	REPOSITORIES+=("ppa:ondrej/php")
	POST_INSTALL_PKGS="$POST_INSTALL_PKGS php$PHP_VERSION "
fi

# Question #3
# install
# TODO: Java installation

echo "Update repo and install"
# Install packages
apt-get update -qqy
apt-get install -qqy "${PRE_INSTALL_PKGS}"

# Add all keys
for i in "${GPG_KEYS[@]}"; do
	curl -fsSL "$i" | apt-key add -
done

# Add all repositories
for i in "${REPOSITORIES[@]}"; do
	add-apt-repository -y -n "$i"
done

# Install packages
apt-get update -qqy
apt-get install -qqy "${POST_INSTALL_PKGS}"

# Docker
echo "Docker post installation steps"

usermod -aG docker $SUDO_USER
curl -fsSL "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
curl -fsSL https://raw.githubusercontent.com/docker/compose/${DOCKER_COMPOSE_VERSION}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose

mkdir -p /etc/docker
cat > /etc/docker/daemon.json <<EOL
{
    "data-root": "/home/docker"
}
EOL

# Snap packages
echo "Snap packages"

snap install --classic "$SNAP_PACKAGES_CLASSIC"
snap install "$SNAP_PACKAGES"

# jetbrains toolbox
# https://download.jetbrains.com/toolbox/jetbrains-toolbox-$JETBRAINS_TOOLBOX_VERSION.tar.gz
xdg-open https://www.jetbrains.com/toolbox-app/

} # this ensures the entire script is downloaded #