#!/bin/bash
# Assumption that this script is used only on ubuntu

{ # this ensures the entire script is downloaded #
set -eu #x

FILENAME=installator.sh

curl -sSL -o ./${FILENAME} https://bitbucket.org/valueadd/ubuntu-installation/raw/master/install.sh
chmod +x ./${FILENAME}
sudo bash ./${FILENAME}

} # this ensures the entire script is downloaded #